const inputArray = ["Rock", "Paper", "Scissors"];

//function returns random int number between min and max
function getRandomFromRange(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

//function returns random element od inputArray
function getComputerChoice() {
  //we use range 0-2 beacouse array starts at 0
  return inputArray[getRandomFromRange(0, 2)];
}

for (let i = 0; i < 100; i++) {
  console.log(getComputerChoice());
}

function playRound(playerSelection, computerSelection) {
  //if both players choose same input its a draw
  if (playerSelection === computerSelection) {
    return "it's a draw! you both chose " + playerSelection;
  }
  //if player wins
  else if (
    (playerSelection === "Rock" && computerSelection === "Scissors") ||
    (playerSelection === "Paper" && computerSelection === "Rock") ||
    (playerSelection === "Scissors" && computerSelection === "Paper")
  ) {
    return `You Won! ${playerSelection} beats ${computerSelection}`;
  }
  //if none of the previous conditions is true than player lost
  else {
    return `You Lose! ${computerSelection} beats ${playerSelection}`;
  }
}
